/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    condition,
    tripetto,
} from "tripetto-runner-foundation";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class FileUploadCondition extends ConditionBlock<{
    readonly isUploaded: boolean;
}> {
    @condition
    isUploaded(): boolean {
        const uploadSlot = this.valueOf<string>();

        return (
            (uploadSlot && uploadSlot.value ? true : false) ===
            this.props.isUploaded
        );
    }
}
