/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    affects,
    definition,
    pgettext,
    tripetto,
} from "tripetto";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_UPLOADED from "../../assets/icon-uploaded.svg";
import ICON_NOT_UPLOADED from "../../assets/icon-not-uploaded.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:file-upload", "File upload state");
    },
})
export class FileUploadCondition extends ConditionBlock {
    @definition
    @affects("#name")
    readonly isUploaded: boolean = true;

    get icon() {
        return this.isUploaded ? ICON_UPLOADED : ICON_NOT_UPLOADED;
    }

    get name() {
        return this.isUploaded
            ? pgettext("block:file-upload", "File uploaded")
            : pgettext("block:file-upload", "No file uploaded");
    }

    get title() {
        return this.node?.label;
    }
}
